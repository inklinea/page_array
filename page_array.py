#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2024] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Page Array - Create a Simple Grid of new pages
# Appears under Extensions>Pages>Page Array
# An Inkscape 1.2+ extension
##############################################################################

import inkex

conversions = {
    'in': 96.0,
    'pt': 1.3333333333333333,
    'px': 1.0,
    'mm': 3.779527559055118,
    'cm': 37.79527559055118,
    'm': 3779.527559055118,
    'km': 3779527.559055118,
    'Q': 0.94488188976378,
    'pc': 16.0,
    'yd': 3456.0,
    'ft': 1152.0,
    '': 1.0,  # Default px
}

def make_page_array(self, width, height, x_spacing, y_spacing, rows, columns):

    x_shift = width + ( x_spacing / self.cf ) * self.spacing_units_cf
    y_shift = 0

    column_count_offset = 1

    for row in range(0, rows):
        for column in range(0, columns-column_count_offset):
            self.svg.namedview.new_page(str(x_shift), str(y_shift), str(width), str(height))
            x_shift = x_shift + width + (x_spacing / self.cf) * self.spacing_units_cf
        x_shift = 0
        column_count_offset = 0
        y_shift = y_shift + height + (y_spacing / self.cf) * self.spacing_units_cf


class PageArray(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--no_row_int", type=int, dest="no_row_int", default=1)

        pars.add_argument("--no_column_int", type=int, dest="no_column_int", default=1)

        pars.add_argument("--row_spacing_float", type=float, dest="row_spacing_float", default=0)

        pars.add_argument("--column_spacing_float", type=float, dest="column_spacing_float", default=0)

        pars.add_argument("--spacing_units_combo", type=str, dest="spacing_units_combo", default='px')
    
    def effect(self):



        self.cf = conversions[self.svg.unit]

        self.spacing_units_cf = conversions[self.options.spacing_units_combo]

        make_page_array(self, self.svg.viewbox_width, self.svg.viewbox_height, x_spacing=self.options.row_spacing_float, y_spacing=self.options.column_spacing_float, rows=self.options.no_row_int, columns=self.options.no_column_int)


if __name__ == '__main__':
    PageArray().run()
